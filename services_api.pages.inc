<?php

/**
 * provide API descriptions for services
 */

/**
 * main services api page with link to the other api pages for each individual service
 */
function services_api_page() {

}

/**
 * describe the given service
 */
function services_api_api_describe($endpoint) {
  $endpoint_url = url($endpoint->path, array('absolute' => TRUE));
  drupal_set_title(t('!name API', array('!name' => ucfirst($endpoint->name))));
  $output = t('Service Endpoint: !path', array('!path' => $endpoint_url)) . '<br/><br/>';
  include_once(drupal_get_path('module', 'services_api') . '/servers/' . $endpoint->server . '.inc');
  $output .= drupal_get_form($endpoint->server . '_services_api_documentation_form', $endpoint);

  $output .= services_api_describe_endpoint($endpoint);
  return '<div class="services-api-documentation ' . $endpoint->name . '">' . $output . '</div>';
}

/**
 * describe the resources and methods of a given endpoint including all arguments
 * and examples if provided
 */
function services_api_describe_endpoint($endpoint) {
  // get the methods for the service
  $content = array();
  $resources = module_invoke_all('services_resources');
  drupal_alter('services_resources', $resources, $endpoint);

  // create a methods array to organize all the methods that the endpoint uses
  $methods = array();
  foreach ($endpoint->resources AS $namespace_key => $namespace) {
    if (isset($namespace['alias']) && !empty($namespace['alias'])) {
      $alias = $namespace['alias'];
    }
    else {
      $alias = FALSE;
    }
    foreach ($namespace AS $resource_name => $resource) {
      foreach ($resource AS $method_name => $method) {
        if ($method['enabled'] && isset($resources[$namespace_key][$method_name])) {
          $methods[$alias ? $alias : $namespace_key][$method_name] = $resources[$namespace_key][$method_name];
        }
        else if ($resource_name == 'actions' && $method['enabled'] && isset($resources[$namespace_key]['actions'][$method_name])) {
          $methods[$alias ? $alias : $namespace_key][$method_name] = $resources[$namespace_key]['actions'][$method_name];
        }
        else if ($resource_name == 'targeted_actions' && $method['enabled'] && isset($resources[$namespace_key]['targeted_actions'][$method_name])) {
          $methods[$alias ? $alias : $namespace_key][$method_name] = $resources[$namespace_key]['targeted_actions'][$method_name];
        }
      }
    }
  }
  ksort($methods);
  foreach ($methods AS $resource_name => $resource) {
    ksort($resource);
    $methods[$resource_name] = $resource;
  }

  $content['methods'] = array();
  foreach ($methods AS $resource_name => $resource) {
    $resource_methods = array();
    $content['methods'][$resource_name] = array(
      '#type'               =>  'fieldset',
      '#title'              =>  $resource_name,
      '#description'        =>  ($resources[$resource_name]['#description'] ? $resources[$resource_name]['#description'] . '<br/>' : '') . t('Click on a method name to view the documentation.'),
      '#collapsible'        =>  TRUE,
      '#collapsed'          =>  FALSE,
    );
    foreach (element_children($resource) AS $method_name) {
      $method = $resource[$method_name];
      $args = array();
      if (!empty($method['args'])) {
        foreach ($method['args'] AS $arg_key => $arg) {
          // occasionally an argument doesn't have a type, so we will assume that it is an int
          if (!isset($arg['type'])) {
            $method['args'][$arg_key]['type'] = $arg['type'] = 'int';
          }

          if ($arg['type'] == 'array' && $method_name == 'create') {
            $arg['description'] = services_api_array_arg($arg, $method_name, $resource_name);
          }
          if ($arg['source'] == 'data' || $arg['source']['data']) {
            $source = t('POST data');
          }
          else if (isset($arg['source']['path'])) {
            $source = t('Path position @position', array('@position' => $arg['source']['path']));
          }
          else if ($arg['source']['param']) {
            $source = t('Parameter "@param"', array('@param' => $arg['source']['param']));
          }
          else {
            $source = 'none';
          }
          $args[] = t(
            '@argname (@type) Source: !source - !description!required',
            array(
              '@argname'      =>  $arg['name'],
              '@type'         =>  $arg['type'],
              '!source'       =>  $source,
              '!description'  =>  $arg['description'],
              '!required'     =>  $arg['optional'] ? '' : ' - <span class="required" >required</span>',
            )
          );
        }
      }
      else {
        $args[] = t('This method has no arguments.');
      }
      $content['methods'][$resource_name][$method_name] = array(
        '#type'             =>  'fieldset',
        '#title'            =>  $method_name,
        '#description'      =>  $method['#description'] ? $method['#description'] : $method['help'],
        '#collapsible'      =>  TRUE,
        '#collapsed'        =>  TRUE,
      );
      $content['methods'][$resource_name][$method_name]['args'] = array(
        '#value'            => theme('item_list', $args, t('Arguments'), 'ul', array('class' => 'method-arguments')),
      );

      if (function_exists($endpoint->server . '_services_api_create_example_request')) {
        $call_example = call_user_func($endpoint->server . '_services_api_create_example_request', $resource_name, $method_name, $endpoint, $method);
      }
      else {
        $call_example = FALSE;
      }

      if (function_exists($endpoint->server . '_services_api_render_response') && isset($method['sample_response']) && !empty($method['sample_response'])) {
        list($response, $response_headers) = call_user_func($endpoint->server . '_services_api_render_response', $method, $method['sample_response']);
        $default_headers = array(
          'Date'    => gmdate('D, d M Y H:i:s T'),
          'Server'  => $_SERVER['SERVER_SOFTWARE'],
        );
        $method['response_headers'] += $response_headers;
        $method['response_headers']['Content-Length'] = strlen($response);
        $method['response_headers'] += $default_headers;
        $headers = array();
        foreach ($method['response_headers'] AS $key => $value) {
          if (is_string($key)) {
            $headers[] = $key . ': ' . $value;
          }
          else {
            $headers[] = $value;
          }
        }
        $response_example = '<pre>' . implode("\n", $headers) . "\n" .  check_plain($response) . '</pre>';
      }
      else {
        $response_example = FALSE;
      }

      if ($call_example || $response_example) {

        if ($call_example) {
          $content['methods'][$resource_name][$method_name]['examples'] = array(
            '#type'             =>  'fieldset',
            '#title'            =>  t('Examples'),
            '#collapsed'        =>  TRUE,
            '#collapsible'      =>  TRUE,
            '#attributes'       => array(
              'class'             => 'examples',
            ),
            '#weight'            => 4,
          );
          $content['methods'][$resource_name][$method_name]['examples']['call_example'] = array(
            '#value'        =>  $call_example,
          );

        }

        if ($response_example) {
          $content['methods'][$resource_name][$method_name]['sample_response'] = array(
            '#type'             =>  'fieldset',
            '#title'            =>  t('Sample Response'),
            '#collapsed'        =>  TRUE,
            '#collapsible'      =>  TRUE,
            '#attributes'       => array(
              'class'             => 'sample_response',
            ),
            '#weight'           => 5,
          );

          $content['methods'][$resource_name][$method_name]['sample_response']['response'] = array(
            '#type'             => 'markup',
            '#value'            => $response_example,
          );
        }
      }
    }
  }
  return drupal_render($content);
}

/**
 * get the arguments for a service with the given endpoint and method
 */
function services_api_get_args($method, $method_name, $resource_name) {
  $args = $method['args'];
  $data_args  = array();
  $path_args  = array();
  $param_args = '';
  if (!empty($args)) {
    foreach ($args as $arg) {
      if ($arg['type'] == 'array' && $method_name == 'create') {
        // create arguments are always coming from POST data
        if (isset($arg['example']) && !empty($arg['example'])) {
          $data_args[$arg['name']] = $arg['example'];
        }
        else {
          $table = services_api_map_api_resource_to_db_table($resource_name);
          $data_args[$arg['name']] = services_api_create_create_arguments($arg, $table);
        }
      }
      else {
        // check out where we are expecting the data to be coming from
        // and assign the data to the correct place
        if ($arg['source'] == 'data' || (is_array($arg['source']) && $arg['source']['data'])) {
          $data_args[$arg['name']] = services_api_example_arg($arg);
        }
        else if (isset($arg['source']['path'])) {
          $path_args[$arg['source']['path']] = services_api_example_arg($arg);
        }
        else if ($arg['source']['param']) {
          $param_args[$arg['source']['param']] = services_api_example_arg($arg);
        }
        else {
          $data_args[$arg['name']] = services_api_example_arg($arg);
        }
      }
    }
  }
  if (!empty($path_args)) {
    $path_args = '/' . implode('/', $path_args);
  }
  else {
    $path_args = FALSE;
  }
  return array($data_args, $path_args, $param_args);
}

/**
 * generate and example arguments based on the 'type' unless there is an example
 * provided in the resource description, then rethrn the provided example
 */
function services_api_example_arg($arg) {
  $type = $arg['type'];
  if (isset($arg['example']) && !empty($arg['example'])) {
    return $arg['example'];
  }
  switch ($type) {
    case 'float':
    case 'double':
      return floatval(rand(0, 99999)/100);
      break;
    case 'init':
    case 'int':
      return intval(rand(0, 99999));
      break;
    case 'string':
      return 'this is a string';
      break;
    case 'struct':
    case 'array':
      return array(
        'key1'    => 'value1',
        'key2'    => 'value2',
        'key3'    => 'value3',
        'array1'  => array(
          'akey1'   => 'avalue1',
          'akey2'   => 'avalue2',
          'akey3'   => 'avalue3',
        ),
      );
      break;
    default:
      return 'abcDefgHijK lMnopQrst UvwXYZ';
      break;
  }
}

/**
 * when an argument is for an array it generally will be a for a database table,
 * let's find out what the keys are for that array
 */
function services_api_array_arg($arg, $method, $resource_name) {
  $args = func_get_args();
  $description = t('An array with some or all of the following fields from the database table. Serial fields are autoincrementing and you should not pass values for them.');

  switch ($method) {
    case 'create':
      $table = services_api_map_api_resource_to_db_table($resource_name);
      break;
  }
  // get the schema for the table
  if (isset($arg['schema']) && !empty($arg['schema'])) {
    $schema = $arg['schema'];
    $description = $arg['description'];
  }
  else {
    $schema = drupal_get_schema($table);
  }

  $array_items = array();
  foreach ($schema['fields'] AS $key => $value) {
    if ($value['type'] == 'varchar') {
      $value['type'] .= ' ' . $value['length'];
    }
    $array_items[] = $key . ' (' . $value['type'] . ') => ' . $value['description'];
  }
  if ($resource_name == 'file' && $method == 'create') {
    $array_items[] = 'file (string) => ' . t('The base64 encoded file contents.');
  }
  return theme('item_list', $array_items, $description);
}

/**
 * when the argument is an array to create a row in the db table we have special
 * handling for the arguments
 */
function services_api_create_create_arguments($arg, $table) {
  $schema = drupal_get_schema($table);
  $fields = array();
  foreach ($schema['fields'] AS $key => $value) {
    if ($value['type'] == 'serial') {
      continue;
    }
    else {
      $fields[] = $key;
    }
  }

  if ($table == 'files') {
    $fields = array(
      'uid',
      'filename',
      'filepath',
      'filemime',
      'filesize',
    );
  }
  $array =  db_fetch_array(db_query('SELECT %s FROM {' . $table . '} LIMIT 1', array(implode(', ', $fields))));

  if ($table == 'files') {
    if (file_exists($array['filepath'])) {
      $array['file'] = base64_encode(file_get_contents($array['filepath']));
    }
    else {
      $array['file'] = base64_encode('this is random text that needs to be base 64 encoded, it needs to be lond to represent an actual filethis is random text that needs to be base 64 encoded, it needs to be lond to represent an actual filethis is random text that needs to be base 64 encoded, it needs to be lond to represent an actual filethis is random text that needs to be base 64 encoded, it needs to be lond to represent an actual filethis is random text that needs to be base 64 encoded, it needs to be lond to represent an actual filethis is random text that needs to be base 64 encoded, it needs to be lond to represent an actual filethis is random text that needs to be base 64 encoded, it needs to be lond to represent an actual filethis is random text that needs to be base 64 encoded, it needs to be lond to represent an actual file');
    }
  }
  return $array;
}

/**
 * given the section of  the api return the database table that is effected
 */
function services_api_map_api_resource_to_db_table($resource_name) {
  switch ($resource_name) {
    case 'comment':
      $table = 'comments';
      break;
    case 'file':
      $table = 'files';
      break;
    case 'taxonomy_term':
      $table = 'term_data';
      break;
    case 'taxonomy_vocabulary':
      $table = 'vocabulary';
      break;
    case 'user';
      $table = 'users';
      break;
    default:
      $table = $resource_name;
      break;
  }
  return $table;
}
