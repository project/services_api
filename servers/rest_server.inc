<?php

/**
 * @file
 *
 * additional documentation for the rest_server services server including
 * example curl calls, and responses
 */

/**
 * Implements hook_services_api_documentation_form().
 */
function rest_server_services_api_documentation_form(&$form_state, $endpoint) {
  global $base_url;

  $form = array();
  global $base_url;
  $formatters = array();
  foreach ($endpoint->server_settings['rest_server']['formatters'] AS $key => $value) {
    if ($value) {
      $formatters[$key] = $key;
    }
  }
  $parsers = array();
  foreach ($endpoint->server_settings['rest_server']['parsers'] AS $key => $value) {
    if ($value) {
      $parsers[$key] = $key;
    }
  }
  $form['header'] = array(
    '#type'             => 'markup',
    '#value'            => t('Here you can view the methods and arguments to use with this endpoint.'),
  );
  $form['generate_examples'] = array(
    '#type'             => 'fieldset',
    '#title'            => t('Generate Examples'),
  );
  $form['generate_examples']['parsers'] = array(
    '#type'             => 'select',
    '#title'            =>  t('Parser'),
    '#description'      =>  t('Which format do you wish to use to send data to the server?'),
    '#options'          =>  $parsers,
    '#default_value'    =>  $_SESSION['rest_server_services_api_parser'],
  );
  if (count($parsers) == 1) {
    $_SESSION['rest_server_services_api_parser'] = array_pop($parsers);
  }
  $form['generate_examples']['formatters'] = array(
    '#type'             => 'select',
    '#title'            =>  t('Formatter'),
    '#description'      =>  t('Which format do you wish to receive information from the server?'),
    '#options'          =>  $formatters,
    '#default_value'    =>  $_SESSION['rest_server_services_api_formatter'],
  );
  if (count($formatters) == 1) {
    $_SESSION['rest_server_services_api_formatter'] = array_pop($formatters);
  }
  $form['generate_examples']['domain']     = array(
    '#type'             => 'textfield',
    '#title'            => t('Domain'),
    '#description'      => t('The domain name to use for the examples'),
    '#default_value'    => $_SESSION['rest_server_services_api_domain'] ? $_SESSION['rest_server_services_api_domain'] : $_SERVER['HTTP_HOST'],
  );
  if (empty($_SESSION['rest_server_services_api_domain'])) {
    $_SESSION['rest_server_services_api_domain'] = $_SERVER['HTTP_HOST'];
  }
  $form['generate_examples']['session']      = array(
    '#type'             => 'textfield',
    '#title'            => t('Session'),
    '#description'      => t('The session string for example'),
    '#default_value'    => $_SESSION['rest_server_services_api_session'] ? $_SESSION['rest_server_services_api_session'] : ''
  );
  $form['generate_examples']['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('Generate Examples'),
  );

  return $form;
}

function rest_server_services_api_documentation_form_submit($form, &$form_state) {
  $_SESSION['rest_server_services_api_parser']    = $form_state['values']['parsers'];
  $_SESSION['rest_server_services_api_formatter'] = $form_state['values']['formatters'];
  $_SESSION['rest_server_services_api_domain']    = $form_state['values']['domain'];
  $_SESSION['rest_server_services_api_session']   = $form_state['values']['session'];
}

/**
 * Implements hook_services_api_render_response().
 */
function rest_server_services_api_render_response($controler, $result) {
  $headers = array();
  $formatters = rest_server_response_formatters();
  $formatter = $formatters[$_SESSION['rest_server_services_api_formatter']];
  if (empty($formatter)) {
    return FALSE;
  }

  // Wrap the results in a model class if required by the formatter
  if (isset($formatter['model'])) {
    $cm = $controller['models'][$formatter['model']];
    if (empty($cm)) {
      return FALSE;
    }
    $model_arguments = isset($cm['arguments'])?$cm['arguments']:array();

    $model_class = new ReflectionClass($cm['class']);
    $result = $model_class->newInstanceArgs(array($result, $model_arguments));
  }

  $view_class = new ReflectionClass($formatter['view']);
  $view_arguments = isset($formatter['view arguments'])?$formatter['view arguments']:array();
  $view = $view_class->newInstanceArgs(array($result, $view_arguments));

  $headers['Content-Type'] = $formatter['mime types'][0];

  return array($view->render(), $headers);
}

/**
 * Implements hook_services_api_create_example_request().
 */
function rest_server_services_api_create_example_request($resource_name, $method_name, $endpoint, $method) {
  $parser     = $_SESSION['rest_server_services_api_parser'];
  $formatter  = $_SESSION['rest_server_services_api_formatter'];
  // only return an example if the response and parsers are set
  if (!empty($_SESSION['rest_server_services_api_parser']) && !empty($_SESSION['rest_server_services_api_formatter'])) {
    global $base_url;
    $examples =  rest_server_create_api_curl_example($resource_name, $method_name, $parser, $formatter, $endpoint, $method);
    if ($_SESSION['rest_server_services_api_domain']) {
      $examples = str_replace($base_url, 'http://' . $_SESSION['rest_server_services_api_domain'], $examples);
    }
    return $examples;
  }
  else {
    return FALSE;
  }
}

/**
 * create an example call to a rest_server implementation
 */
function rest_server_create_api_curl_example($resource_name, $method_name, $parser, $formatter, $endpoint, $method) {
  switch ($method_name) {
    case 'update':
      $request_type = 'PUT';
      $crud = TRUE;
      break;
    case 'retrieve':
    case 'index':
      $request_type = 'GET';
      $crud = TRUE;
      break;
    case 'delete':
      $request_type = 'DELETE';
      $crud = TRUE;
      break;
    case 'create':
      $crud = TRUE;
    default:
      $request_type = 'POST';
      break;
  }
  $auth = '';
  if (isset($endpoint->authentication['services']) && $endpoint->authentication['services'] == 'services') {
    // session based authentication
    if ($resource_name == 'user' && $method_name == 'login') {
      // we don't set a cookie when using login
    }
    else {
      $auth = ' -b "' . ($_SESSION['rest_server_services_api_session'] ? $_SESSION['rest_server_services_api_session'] : session_name() . '=' . session_id()) . '" ';
    }
  }

  list($data_args, $path_args, $param_args) = services_api_get_args($method, $method_name, $resource_name);
  if (!empty($data_args)) {
    $data_args = rest_server_format_args($parser, $data_args);
  }
  $content_type = '-H "Content-Type: ' . $parser . '"';
  if ($parser == 'application/x-www-form-urlencoded') {
    $temp_args = explode('&', $temp_args);
    $args = '';
    if (!empty($data_args)) {
      foreach ($data_args AS $arg) {
        $data_args .= ' --data ' . $arg;
      }
    }

    $content_type = '';
  }
  else if (!empty($data_args)) {
    $data_args = " --data '$data_args'";
  }
  $example = 'curl -X ' . $request_type . ' ' . $auth . ' ' . ($content_type ? $content_type : '') . ' ' .  ($data_args ? $data_args : '') . ' "' . url($endpoint->path . '/' . $resource_name . ($crud ? '' : '/' . $method_name) . ($path_args ? $path_args : '') . '.' . $formatter, array('absolute' => TRUE, 'query' => $param_args)) . '"';
  return '<strong>' . t('Use curl') . '</strong><br/><code>' . $example . '</code>';
}

/**
 * format the arguments for a rest_server example call
 */
function rest_server_format_args($parser, $args) {
  switch($parser) {
    case 'application/json':
      return json_encode($args);
      break;
    case 'application/vnd.php.serialized':
      return serialize($args);
      break;
    case 'application/x-yaml':
      return 'some yaml data goes here';
      break;
    case 'multipart/form-data':
      return 'some multipart form data goes here';
      break;
    case 'application/x-www-form-urlencoded':
    default:
    return drupal_query_string_encode($args);
      break;
  }
}
