<?php

/**
 * @file
 *
 * describe all the built in service methods
 *
 * in theory we will describe the resources as well as each method, if this
 * doesn't conflice with the services module
 */

/**
 * describe the file methods
 */
function services_api_get_file_resource_alter(&$resources) {
  if (!isset($resources['file']['#description'])) {
    //$resources['file']['help'] = t('Any file being tracked by the system. Files are inserted into the {files} table in the database and are associated with the user who uploaded it.');
  }

  if (!isset($resources['file']['create']['#description'])) {
    $resources['file']['create']['#description'] = t('Upload the file to the server and create the necessary entry into the database relating the file to the user.');
  }

  if (!isset($resources['file']['retrieve']['#description'])) {
    $resources['file']['retrieve']['#description'] = t('Retrieve the file from the server.');
  }

  if (!isset($resources['file']['delete']['#description'])) {
    $resources['file']['delete']['#description'] = t('Delete the file from the database, also removes the file from the server file system.');
  }
}

/**
 * describe the comment methods
 */
function services_api_get_comment_resource_alter(&$resources) {
  if (!isset($resources['comment']['#description'])) {
    //$resources['comment']['#description'] = t('Comments can be made to post, these are stored in the database and displayed when the post is displayed.');
  }

  if (!isset($resources['comment']['create']['#description'])) {
    $resources['comment']['create']['#description'] = t('Create the comment in the database relating it to the user who wrote the comment and the post being commented upon. Comments can also be replies to previous comments and can have a position in a thread.');
  }

  if (!isset($resources['comment']['retrieve']['#description'])) {
    $resources['comment']['retrieve']['#description'] = t('Retrieve a comment from the database.');
  }

  if (!isset($resources['comment']['delete']['#description'])) {
    $resources['comment']['delete']['#description'] = t('Delete a comment from the database.');
  }

/*
  if (!isset($resources['comment']['actions']['countAll']['#description'])) {
    $resources['comment']['actions']['countAll']['#description'] = t('');
  }

  if (!isset($resources['comment']['actions']['countNew']['#description'])) {
    $resources['comment']['actions']['countNew']['#description'] = t(');
  }
*/
}

/**
 * describe the node methods
 */
function services_api_get_node_resource_alter(&$resources) {
  if (!isset($resources['node']['#description'])) {
    //$resources['node']['#description'] = t('Nodes are posts made on the system. They can be blog posts, news articles, or photos and videos with descriptions.');
  }

  if (!isset($resources['node']['create']['#description'])) {
    $resources['node']['create']['#description'] = t('Create a new post on the system.');
  }

  if (!isset($resources['node']['retrieve']['#description'])) {
    $resources['node']['retrieve']['#description'] = t('Retrieve a post from the database.');
  }

  if (!isset($resources['node']['delete']['#description'])) {
    $resources['node']['delete']['#description'] = t('Delete a post from the database.');
  }
}

/**
 * describe user methods
 */
function services_api_get_user_resource_alter(&$resources) {
  //add API documentation for user resource methods
  if ($resources['user']['actions']['register']) {
    $resources['user']['actions']['register'] += array(
      'sample_response' => array(
        'sessid' => '8bc2a1b4386150adf94a87f256a5f289',
        'session_name' => 'SESS234d0ffbd6c17e8ca8c0f87f79da11c2',
        'user' => array(
          'uid' => '1234',
          'name' => 'testaccount1234',
          'mail' => 'testaccount@test.com',
          'init' => 'testaccount@test.com',
          'create' => '123456789',
          'access' => '123456789',
          'login' => '123456789',
          'status' => '1',
          'timezone' => '',
          'birthday' => '123456789',
          'terms' => '123456789',
          'roles' => array(
            '2' => 'authenticated user',
            '6' => 'social user',
            '9' => 'adult'
          ),
        ),
      ),
      'response_headers'  => array(
        'HTTP/1.1 200 OK',
      ),
    );
  }
  if ($resources['user']['actions']['login']) {
    $resources['user']['actions']['login'] += array(
      'sample_response' => array(
        'sessid' => '8bc2a1b4386150adf94a87f256a5f289',
        'session_name' => 'SESS234d0ffbd6c17e8ca8c0f87f79da11c2',
        'user' => array(
          'uid' => '1234',
          'name' => 'testaccount1234',
          'mail' => 'testaccount@test.com',
          'init' => 'testaccount@test.com',
          'create' => '123456789',
          'access' => '123456789',
          'login' => '123456789',
          'status' => '1',
          'timezone' => '',
          'birthday' => '123456789',
          'terms' => '123456789',
          'roles' => array(
            '2' => 'authenticated user',
            '6' => 'social user',
            '9' => 'adult'
          ),
        ),
      ),
      'response_headers'  => array(
        'HTTP/1.1 200 OK',
      ),
    );
  }
  if ($resources['user']['actions']['logout']) {
    $resources['user']['actions']['logout'] += array(
      'sample_response' => TRUE,
      'response_headers'  => array(
        'HTTP/1.1 200 OK',
        'Set-Cookie'    => session_name() . '=deleted; expires=' . gmdate('D, d-M-Y H:i:s T', time() -1) . '; path=/; httponly',
        'Cache-Control' => 'no-cache, must-revalidate, post-check=0, pre-check=0',
      ),
    );
  }
}
