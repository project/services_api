<?php

/**
 * @file
 *
 * hooks that are provided by the services_api module
 */

/**
 * a form to give the user the option to set settings for the examples provided
 * in the documentation. Also provide you own submit function that saves the
 * settings in the $_SESSION.
 *
 * @see rest_server_services_api_documentation_form()
 */
function hook_services_api_documentation_form(&$form_state, $endpoint) {

}

/**
 * render the example responses provided
 *
 * @see rest_server_services_api_render_response()
 */
function hook_services_api_render_response($controler, $formatter, $result) {

}

/**
 * create a sample request to this endpoint for the resource and method
 *
 * @see rest_server_services_api_create_example_request().
 */
function hook_services_api_create_example_request($resource_name, $method_name, $parser, $formatter, $endpoint, $method) {

}
